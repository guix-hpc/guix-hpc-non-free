;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2020, 2022, 2024 Inria

(define-module (hacky suitesparse-mkl)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages maths)
  #:use-module (srfi srfi-1)
  #:use-module (guix-science-nonfree packages mkl))

(define-public suitesparse-mkl
  (package
   (inherit suitesparse)
   (name "suitesparse-mkl")
   (inputs
    (modify-inputs (package-inputs suitesparse)
      (delete "openblas")
      (prepend intel-oneapi-mkl libomp)))
   (arguments
    `(#:modules ((guix build utils) (guix build gnu-build-system) (srfi srfi-1))
      ,@(substitute-keyword-arguments
         (package-arguments suitesparse)
         ((#:make-flags flags)
          `(cons
            (string-append "MKLROOT="
                           (assoc-ref %build-inputs "intel-oneapi-mkl"))
            (lset-difference string=? ,flags
                             (list "BLAS=-lopenblas" "LAPACK=-lopenblas"))))
         )))
   (synopsis "Suite of sparse matrix software (compiled with Intel® MKL instead
of OpenBLAS)")))
