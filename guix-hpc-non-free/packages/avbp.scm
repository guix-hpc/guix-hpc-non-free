;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages avbp)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages version-control)
  #:use-module (non-free parmetis))

(define avbp-source-environment-variable "AVBP_GIT_REPO")

(define avbp-license-environment-variable "AVBP_LIBSUP")

(define avbp-test-suite-environment-variable "AVBP_TEST_SUITE")

(define %avbp-standard-test-suite
  ;; List of test sub-directories to be used for the standard AVBP
  ;; version.
  (list "1D_FLAME_AI_IPRS"
        "1D_FLAME_ARC_LDSC"
        "1D_FLAME_EE"
        "1D_FLAME_EL_MULTICOMPONENT"
        "1D_FLAME_PGS_ARC"
        "1D_FLAME_SOOT_EL"
        "1D_FLAME_SOOT_SECT"
        "1D_HOQ"
        "1D_SHOCK_TUBE"
        "2D_DUAL_FUEL_MIXER"
        "2D_RIEMANN"
        "3D_FLAME_INHIBITOR"
        "3D_PLANAR_FLAME"
        "COVO"
        "EL_BREAKUP_FILM_PAMELA"
        "EL_MUSTARD"
        "ENACCEF2"
        "GPU_PRECCINSTA"
        "HYLON"
        "IGNITION_DEPOSIT"
        "IGNITION_SOFT"
        "NASAC3XG1"
        "PANCAKE"
        "PRECCINSTA"
        "TURBINJ_SECTOR"
        "VOLVO"))

(define avbp-license
  (package
    (name "avbp-license")
    (version "1")
    (source
     (let* ((avbp-license (getenv avbp-license-environment-variable))
            (folder (and avbp-license
                         (file-exists? avbp-license)
                         (canonicalize-path avbp-license))))
       (and folder
            (local-file folder "avbp-license-libraries"
                        #:recursive? #t
                        #:select?                 ;preserve only libsup7_*.so
                        (lambda (file stat)
                          (and (string-prefix? "libsup7_" (basename file))
                               (string-suffix? ".so" file)
                               (eq? 'regular (stat:type stat))))))))
    (build-system trivial-build-system)
    (arguments
      (list #:modules '((guix build utils))
            #:builder #~(begin
                          (use-modules (guix build utils))
                          (when (not #$(package-source this-package))
                            (format #t
                                    "Please provide the path to the folder containing the license file by setting the environment variable ~a.~%Check the path if the environment variable is set.~%"
                                    #$avbp-license-environment-variable)
                            (exit 1))
                          (let* ((license-file "libsup7_gnu.so")
                                 (source-file (string-append #$(package-source this-package) "/" license-file))
                                 (lib (string-append #$output "/lib"))
                                 (target (string-append lib "/" license-file)))
                            (unless (file-exists? source-file)
                              (format #t
                                      "License file \"~a\" not found. Please ensure that the license file is present in the license folder.~%"
                                      license-file)
                              (exit 1))
                            (mkdir-p lib)
                            (copy-file source-file target)))))
    (synopsis "AVBP license package")
    (description "This is a private package used by AVBP to specify the license.")
    (home-page "https://www.cerfacs.fr/avbp7x/index.php")
    ;; Proprietary component of AVBP.
    (license #f)))

(define avbp-test-suite
  (package
    (name "avbp-test-suite")
    (version "1")
    (source
     (let ((avbp-test-suite (getenv avbp-test-suite-environment-variable)))
              (and avbp-test-suite
                   (file-exists? avbp-test-suite)
                   ;; use local-file since there is no point in using a specific commit from the test case repo.
                   (local-file (canonicalize-path avbp-test-suite)
                               #:recursive? #t
                               #:select? (lambda (file stat)
                                           (any (lambda (test)
                                                  (or (equal? test
                                                              (basename file))
                                                      (string-contains file
                                                                       (string-append "/" test "/"))))
                                                %avbp-standard-test-suite))))))
    (build-system trivial-build-system)
    (arguments
      (list #:modules '((guix build utils))
            #:builder #~(begin
                          (use-modules (guix build utils))
                          (unless #$(package-source this-package)
                            (format #t
                                    "No test suite provided through the environment variable ~a, nothing to build.~%"
                                    #$avbp-test-suite-environment-variable)
                            (exit #f))
                          (copy-recursively #$(package-source this-package) #$output))))
    (synopsis "AVBP test stuite")
    (description "This is a private package used by AVBP to provide the test suite.")
    (home-page "https://www.cerfacs.fr/avbp7x/index.php")
    ;; Proprietary component of AVBP.
    (license #f)))

(define (test-runner avbp test-suite)
  "Return a program as a file-like object to run the tests defined by
@var{test-suite} using @var{abvp}."
  (define run-tests
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (ice-9 match)
                       (srfi srfi-1)
                       (srfi srfi-34))

          (let* ((testdir (match (command-line)
                            ((argv0 argv1)
                             argv1)
                            ((argv0)
                             "/tmp")
                            ((argv0 _ ...)
                             (format #t "Usage: ~a [DIRECTORY]~%~%Run AVBP tests after copying them to DIRECTORY (/tmp by default).~%"
                                     argv0)
                             (exit 1))))
                 (tmpdir (mkdtemp (string-append testdir "/avbp-tests-XXXXXX")))
                 (tests #$test-suite)
                 (avbp-exe (string-append #$avbp "/bin/AVBP_V"
                                          #$(package-version avbp))))
            ;; Copy test suite to test directory as tests can't be run from the store directory.
            (copy-recursively tests tmpdir)
            (define (s-trim s)
              "Trims /RUN from the end of the string S."
              (let* ((start (- (string-length s) 4))
                     (start (or (and (positive? start) start) 0))
                     (s-index (string-contains s "/RUN" start)))
                (or (and s-index (string-drop-right s 4)) S)))
            (let* ((test-results
                    (map
                     (lambda (directory)
                       ;; The binary has to be run from within
                       ;; the testcase directory where it will
                       ;; pick the necessary input files.
                       (let ((test-name (s-trim
                                         (string-drop directory
                                                      (+ 1 (string-length tmpdir))))))
                         (with-directory-excursion directory
                           (begin
                             (format #t
                                     "Running test ~a~%"
                                     test-name)
                             (force-output)
                             (let ((exit-val (status:exit-val (system* avbp-exe))))
                               (if (and exit-val
                                        (= 0 exit-val))
                                   (begin
                                     (format #t "Test successful~%")
                                     `(#t . ,test-name))
                                   (begin
                                     (format #t "Test failed~%")
                                     `(#f . ,test-name))))))))
                     (find-files tmpdir
                                 (lambda (name stat)
                                   ;; Filter directories containing a test.
                                   (and (eq? (stat:type stat)
                                             'directory)
                                        (file-exists? (string-append name "/run.params"))))
                                 #:directories? #t)))
                   (successful-tests (filter-map
                                      (match-lambda
                                        ((success? . name)
                                         (and success?
                                              name)))
                                      test-results))
                   (failed-tests (filter-map
                                  (match-lambda
                                    ((success? . name)
                                     (and (not success?)
                                          name)))
                                  test-results)))
              (format #t "~%---------------------------------------------------------------~%")
              (format #t "TESTS SUMMARY~%")
              (unless (null? successful-tests)
                (format #t "List of successful tests:~%- ~a~%" (string-join successful-tests
                                                                            "\n- ")))
              (unless (null? failed-tests)
                (format #t "List of failed tests:~%- ~a~%" (string-join failed-tests "\n- "))
                (exit EXIT_FAILURE)))))))

  (program-file "run-avbp-tests" run-tests))

(define-public avbp-tests
  (package
    (name "avbp-tests")
    (version "1")
    (source #f)
    (build-system gnu-build-system)
    (native-inputs (list openmpi))
    (inputs (list avbp
                  avbp-test-suite))
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               (delete 'unpack)
               (delete 'configure)
               (delete 'build)
               (replace 'install
                 (lambda _
                   (let* ((bin (string-append #$output "/bin"))
                          (avbp-tests (string-append bin "/avbp-tests")))
                     (mkdir-p bin)
                     (symlink #$(test-runner (this-package-input "avbp")
                                             (this-package-input "avbp-test-suite"))
                              avbp-tests))))
               (add-before 'check 'openmpi-setup
                 #$%openmpi-setup)
               (replace 'check
                 (lambda* (#:key tests? #:allow-other-keys)
                   (when tests?
                     (invoke #+(test-runner (this-package-input "avbp")
                                            (this-package-input "avbp-test-suite")))))))))
    (synopsis "AVBP test package")
    (description "This is a package used to run an AVBP test suite.")
    (home-page "https://www.cerfacs.fr/avbp7x/index.php")
    ;; Proprietary component of AVBP.
    (license #f)))

(define-public avbp
  (package
    (name "avbp")
    (version "7.14.2")
    (source
     (let ((avbp-source (getenv avbp-source-environment-variable)))
       (and avbp-source
            (git-checkout
             (url (string-append "file://" (canonicalize-path avbp-source)))
             (commit version)))))
    (build-system cmake-build-system)
    (arguments
     (list #:substitutable? #f
           ;; Disable parallel build to fix non-reproducible build
           ;; failure.
           #:parallel-build? #f
           #:phases #~(modify-phases %standard-phases
                        (add-before 'unpack 'check-source
                          (lambda _
                            (unless #$(package-source this-package)
                              (format #t
                                      "No source is defined. Please provide the path to a local clone of the AVBP Git repository by setting the environment variable ~a.~%"
                                      #$avbp-source-environment-variable)
                              (exit 1))))

                        ;; Hardcode Git related variables to produce fixed output.
                        (add-after 'unpack 'fix-git-string-generation
                          (lambda _
                            (let ((version #$(package-version this-package)))
                              (substitute* "LIBRARIES/cmake/create_git_version.cmake"
                                (("^STRING\\(REPLACE.*\n$")
                                 (format #f
                                         "set (GIT_VERSION_STRING \"~a\" )~%
set (GIT_VERSION_EXE_STRING \"~a\" )~%
set (GIT_DATE_STRING \"'TODAY'\" )~%
set (GIT_BRANCH_NAME \"\" )~%"
                                         version
                                         version))))))

                        ;; Properly set up the license file path.
                        (add-after 'unpack 'fix-license-file-search
                          (lambda _
                            (substitute* "LIBRARIES/cmake/FindAVBP_LICENSE.cmake"
                              (("\\$ENV\\{AVBP_LIBSUP\\}") (string-append #$avbp-license "/lib")))))

                        ;; Rename the final avbp executable for easier wrapping.
                        (add-before 'configure 'rename-avbp-executable
                          (lambda _
                            (substitute* "CMakeLists.txt"
                              (("set \\(AVBPEXE.*")
                               (format #f "set (AVBPEXE \"AVBP_V~a\")~%" #$(package-version this-package))))))
                        ;; The CMake files redefine for each install
                        ;; target the destination directory, not
                        ;; honouring $PREFIX.
                        (add-before 'configure 'fix-cmake-install-targets
                          (lambda _
                            (let* ((bin (string-append #$output "/bin")))
                              (mkdir-p bin)
                              (for-each (lambda (file)
                                          (substitute* file
                                            (("^install\\((TARGETS .*) DESTINATION.*" _ target)
                                             (format #f "install (~a)~%" target))))
                                        (find-files "." "CMakeLists.txt$")))))

                        (add-before 'check 'mpi-setup
                          #$%openmpi-setup)

                        ;; Use test provided in the repo.
                        (replace 'check
                          (lambda* (#:key tests? #:allow-other-keys)
                            (when tests?
                              (let* ((tmpdir (getenv "TMPDIR"))
                                     (default-test-dir (string-append tmpdir "/source/TESTCASE/COVO/RUN"))
                                     (avbp-exe (string-append tmpdir "/build/AVBP_V" #$(package-version this-package))))
                                (setenv "AVBP_HOME" (string-append tmpdir "/source/WORK"))
                                (with-directory-excursion default-test-dir
                                  (invoke avbp-exe))
                                (unsetenv "AVBP_HOME")))))

                        ;; Copy additional files and wrap program.
                        (add-after 'install 'install-work-files-and-wrap-program
                          (lambda _
                            (let* ((avbp-share-dir (string-append #$output "/share/avbp"))
                                   (avbp-bin-dir (string-append #$output "/bin"))
                                   (avbp-exe (string-append avbp-bin-dir "/AVBP_V" #$(package-version this-package))))
                              (mkdir-p avbp-share-dir)
                              (copy-recursively (string-append #$(package-source this-package) "/WORK")
                                                (string-append avbp-share-dir "/WORK"))
                              (wrap-program avbp-exe
                                `("AVBP_HOME" = (,avbp-share-dir)))
                              (symlink avbp-exe (string-append avbp-bin-dir "/avbp")))))
                        (add-after 'install 'install-tools
                          (lambda _
                            (let* ((tmpdir (getenv "TMPDIR"))
                                   (avbp-share-dir (string-append #$output "/share/avbp"))
                                   (avbp-bin-dir (string-append #$output "/bin"))
                                   (avbp-exe (string-append avbp-bin-dir "/AVBP_V" #$(package-version this-package)))
                                   (choices-dir (string-append avbp-share-dir "/CHOICES"))
                                   (scripts-dir (string-append tmpdir "/source/TOOLS/BIN/SCRIPTS")))
                              (copy-recursively (string-append #$(package-source this-package) "/TOOLS/BIN/CHOICES")
                                                choices-dir)
                              (substitute* (find-files scripts-dir)
                                (("^(EXEDIR=).*" _ var)
                                 (string-append var avbp-bin-dir))
                                (("^(CHOICESDIR=).*" _ var)
                                 (string-append var choices-dir)))
                              (copy-recursively scripts-dir
                                                avbp-bin-dir)))))))
    (inputs (list hdf5-parallel-openmpi
                  (list hdf5-parallel-openmpi "fortran")
                  metis4parmetis-r64
                  openblas
                  parmetis-r64
                  perl           ;; for tools
                  python-wrapper ;; for tools
                  avbp-license))
    (native-inputs (list gfortran
                         git          ; only needed for version number
                         openmpi))
    (properties '((tunable? . #t)))
    (synopsis "3D Navier-Stokes compressible DNS/LES solver")
    (description "AVBP is a 3D Navier-Stokes compressible DNS/LES solver co-developped
by CERFACS and IFP-EN until version 7.3. Since, CERFACS ensures the
alone development and improvement of the code.")
    (home-page "https://www.cerfacs.fr/avbp7x/index.php")
    ;; Closed source software.
    (license #f)))
