;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages utils)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix-hpc packages utils)
  #:use-module (guix-science-nonfree packages cuda))

(define (make-ginkgo-cuda name cuda-package)
  (package/inherit ginkgo
    (name name)
    (arguments (substitute-keyword-arguments (package-arguments ginkgo)
                 ((#:configure-flags flags)
                  ;; We don't need to specify an architecture as by default
                  ;; ginkgo builds for all architectures supported by the
                  ;; provided CUDA version
                  #~(append (list "-DGINKGO_BUILD_CUDA=ON")
                            #$flags))
                 ;; Cannot run tests due to lack of specific hardware
                 ((#:tests? _ #t)
                  #f)
                 ;; RUNPATH validation fails since libcuda.so.1 is not present at build
                 ;; time.
                 ((#:validate-runpath? #f #f)
                  #f)))
    (inputs (modify-inputs (package-inputs ginkgo)
              (prepend cuda-package)))))

(define-public ginkgo-cuda-11
  (make-ginkgo-cuda "ginkgo-cuda-11" cuda-11))

(define-public ginkgo-cuda-12
  (make-ginkgo-cuda "ginkgo-cuda-12" cuda))
